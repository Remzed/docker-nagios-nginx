#!/bin/bash


htpasswd -c -b -s "${NAGIOS_HOME}/etc/htpasswd.users" "${NAGIOSADMIN_USER}" "${NAGIOSADMIN_PASS}"
chown -R nagios.nagios "${NAGIOS_HOME}/etc/htpasswd.users"

sed -i "s/nagios@email.ltd/${NAGIOS_EMAIL}/g" "${NAGIOS_HOME}"/etc/objects/commands.cfg

sed -i "s/dsi@email.ltd/${NAGIOS_EMAIL_DEST}/g" "${NAGIOS_HOME}"/etc/objects/contacts.cfg

# Need fix acl 
# https://www.servernoobs.com/error-could-not-open-command-file-usrlocalnagiosvarrwnagios-cmd-for-update/
# chown www-data:www-data /opt/nagios/var/rw/*
echo "* * * * * chown www-data:www-data /opt/nagios/var/rw/*" > /opt/crontab

crontab /opt/crontab

/usr/sbin/cron

service nginx start

service php5-fpm start

service postfix start

service fcgiwrap start 

exec "${NAGIOS_HOME}/bin/nagios" "${NAGIOS_HOME}/etc/nagios.cfg"





