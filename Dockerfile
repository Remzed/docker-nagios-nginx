FROM ubuntu:14.04
MAINTAINER Remy Zygas <r.zygas@gmail.com>

ENV NAGIOS_HOME               /opt/nagios
ENV NAGIOS_USER               nagios
ENV NAGIOS_GROUP              nagios
ENV NAGIOS_CMDUSER            nagios
ENV NAGIOS_CMDGROUP           nagios
ENV NAGIOS_FQDN               nagios
ENV NAGIOS_EMAIL              nagios@email.ltd
ENV NAGIOS_EMAIL_DEST         sup@email.ltd
ENV NAGIOSADMIN_USER          nagiosadmin
ENV NAGIOSADMIN_PASS          nagiospasswd
ENV NAGIOS_TIMEZONE           UTC
ENV DEBIAN_FRONTEND           noninteractive
ENV NG_NAGIOS_CONFIG_FILE     ${NAGIOS_HOME}/etc/nagios.cfg
ENV NG_CGI_DIR                ${NAGIOS_HOME}/sbin
ENV NG_CGI_URL                /cgi-bin
ENV NAGIOS_BRANCH             nagios-4.3.4
ENV NAGIOS_PLUGINS_BRANCH     release-2.2.1
ENV NRPE_BRANCH               nrpe-3.2.1


RUN echo postfix postfix/main_mailer_type string "'Internet Site'" | debconf-set-selections           && \
    echo postfix postfix/mynetworks string "127.0.0.0/8" | debconf-set-selections                     && \
    echo postfix postfix/mailname string ${NAGIOS_FQDN} | debconf-set-selections                      && \
    apt update && apt install -y --force-yes git build-essential openssl libssl-dev autotools-dev automake gettext nginx unzip libgd2-xpm-dev php5-fpm spawn-fcgi fcgiwrap apache2-utils postfix mailutils

RUN ( egrep -i "^${NAGIOS_GROUP}"    /etc/group || groupadd $NAGIOS_GROUP    )                        && \
    ( egrep -i "^${NAGIOS_CMDGROUP}" /etc/group || groupadd $NAGIOS_CMDGROUP )
RUN ( id -u $NAGIOS_USER    || useradd --system -d $NAGIOS_HOME -g $NAGIOS_GROUP    $NAGIOS_USER    ) && \
    ( id -u $NAGIOS_CMDUSER || useradd --system -d $NAGIOS_HOME -g $NAGIOS_CMDGROUP $NAGIOS_CMDUSER )

RUN cd /tmp                                                                                           && \
    git clone https://github.com/NagiosEnterprises/nagioscore.git -b $NAGIOS_BRANCH                   && \
    cd nagioscore                                                                                     && \
    ./configure                                                                                          \
        --prefix=${NAGIOS_HOME}                                                                          \
        --exec-prefix=${NAGIOS_HOME}                                                                     \
        --enable-event-broker                                                                            \
        --with-command-user=${NAGIOS_CMDUSER}                                                            \
        --with-command-group=${NAGIOS_CMDGROUP}                                                          \
        --with-nagios-user=${NAGIOS_USER}                                                                \
        --with-nagios-group=${NAGIOS_GROUP}                                                              \
                                                                                                      && \
    make all                                                                                          && \
    make install                                                                                      && \
    make install-config                                                                               && \
    make install-commandmode                                                                          && \
    make clean

RUN cd /tmp                                                                                           && \
    git clone https://github.com/nagios-plugins/nagios-plugins.git -b $NAGIOS_PLUGINS_BRANCH          && \
    cd nagios-plugins                                                                                 && \
    ./tools/setup                                                                                     && \
    ./configure                  \
        --prefix=${NAGIOS_HOME}  \
                                                                                                      && \
    make                                                                                              && \
    make install                                                                                      && \
    make clean                                                                                        && \
    mkdir -p /usr/lib/nagios/plugins                                                                  && \
    ln -sf /opt/nagios/libexec/utils.pm /usr/lib/nagios/plugins

RUN cd /tmp                                                                                           && \
    git clone https://github.com/NagiosEnterprises/nrpe.git -b $NRPE_BRANCH                           && \
    cd nrpe                                                                                           && \
    ./configure                                                                                          \
        --with-ssl=/usr/bin/openssl                                                                      \
        --with-ssl-lib=/usr/lib/x86_64-linux-gnu                                                         \
                                                                                                      && \
    make check_nrpe                                                                                   && \
    cp src/check_nrpe ${NAGIOS_HOME}/libexec/                                                         && \
    make clean

RUN htpasswd -c -b /opt/nagios/etc/htpasswd.users ${NAGIOSADMIN_USER} ${NAGIOSADMIN_PASS}

COPY config/nagios.conf /etc/nginx/conf.d/nagios.conf
COPY config/nginx.conf /etc/nginx/nginx.conf 
COPY config/fastcgi_params /etc/nginx/fastcgi_params
COPY config/main.cf /etc/postfix/main.cf
COPY config/sasl_passwd /etc/postfix/sasl_passwd
COPY config/commands.cfg "${NAGIOS_HOME}"/etc/objects/commands.cfg
COPY config/localhost.cfg "${NAGIOS_HOME}"/etc/objects/localhost.cfg

RUN echo "cfg_dir=/opt/nagios/etc/additional_conf" >> /opt/nagios/etc/nagios.cfg

RUN sed -i "s/nagios@localhost/${NAGIOS_EMAIL_DEST}/g" "${NAGIOS_HOME}"/etc/objects/contacts.cfg

RUN sed -i "s/interval_length=60/interval_length=15/g" "${NAGIOS_HOME}"/etc/nagios.cfg

# Need to add your own sasl_passwd
# RUN cd /etc/postfix                                                                                  && \
#     postmap sasl_passwd

COPY start_nagios.sh /usr/local/bin/start_nagios

RUN chmod +x /usr/local/bin/start_nagios

RUN mkdir -p /opt/nagios/etc/additional_conf

VOLUME "/opt/nagios/etc/additional_conf" 

EXPOSE 80

ENTRYPOINT ["/usr/local/bin/start_nagios"]
    
