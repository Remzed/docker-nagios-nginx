#!/bin/bash
set -e

DOCKER_RUN_IMAGE=nagios
NAGIOS_EMAIL=nagios@email.ltd
NAGIOS_EMAIL_DEST=sup@email.ltd
NAGIOS_VOLUME="nagios_nginx/additional_conf"

mkdir -p ${NAGIOS_VOLUME}

docker build -t "${DOCKER_RUN_IMAGE}" .
docker images
docker run -d --name "${DOCKER_RUN_IMAGE}" -e NAGIOS_EMAIL="${NAGIOS_EMAIL}" -e NAGIOS_EMAIL_DEST="${NAGIOS_EMAIL_DEST}"  -p 80:80 -v "${NAGIOS_VOLUME}":/opt/nagios/etc/additional_conf -t "${DOCKER_RUN_IMAGE}"

docker ps -a

